﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class FrameRate : MonoBehaviour {

	[SerializeField]
	private RenderTexture _renderTexture = null;
	[SerializeField]
	private VideoClip _videoClip = null;

	private IEnumerator Start()
	{
		
		Application.runInBackground = true;

		//var camera = Camera.main.gameObject;
		var videoPlayer = gameObject.AddComponent<VideoPlayer>();
		var audioSource = gameObject.AddComponent<AudioSource>();

		videoPlayer.playOnAwake = false;
		audioSource.playOnAwake = false;

		videoPlayer.source = VideoSource.VideoClip;
		videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
		videoPlayer.renderMode = VideoRenderMode.RenderTexture;
		videoPlayer.EnableAudioTrack(0, true);
		videoPlayer.SetTargetAudioSource(0, audioSource);
		videoPlayer.clip = _videoClip;
		videoPlayer.Prepare();

		while (!videoPlayer.isPrepared)
			yield return null;

		videoPlayer.targetTexture = _renderTexture;
		audioSource.Play();
		videoPlayer.Play();



		while (videoPlayer.isPlaying)
			yield return null;

		//ScreenFader.FadeIn(2.5f, () => SceneManager.LoadScene("Menu"));
	}

}
